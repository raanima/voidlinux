# VOID INSTALL

    cfdisk /dev/sda  
    mkfs -t ext2 /dev/sda1
    mkfs -t ext4 /dev/sda2
    mkfs -t ext4 /dev/sda3
    mkswap /dev/sda4
    swapon /dev/sda4

    mount /dev/sda2 /mnt
    mkdir /mnt/boot
    mkdir /mnt/home
    mount /dev/sda1 /mnt/boot
    mount /dev/sda3 /mnt/home  
  
    REPO=https://repo-default.voidlinux.org/current  
    ARCH=x86_64  
    mkdir -p /mnt/var/db/xbps/keys  
    cp /var/db/xbps/keys/* /mnt/var/db/xbps/keys/  
  
    XBPS_ARCH=$ARCH xbps-install -S -r /mnt -R "$REPO" base-system  
  
    mount --rbind /sys /mnt/sys && mount --make-rslave /mnt/sys  
    mount --rbind /dev /mnt/dev && mount --make-rslave /mnt/dev  
    mount --rbind /proc /mnt/proc && mount --make-rslave /mnt/proc  
  
    cp /etc/resolv.conf /mnt/etc/  
  
    xchroot /mnt /bin/bash  
  
    xbps-install nano  
    nano /etc/default/libc-locales  
    xbps-reconfigure -f glibc-locales  
  
    ln -sf /usr/share/zoneinfo/Europa/Minsk /etc/localtime  
   
    useradd -m -g users -G wheel -s /bin/bash admin
    passwd admin

    useradd -m -g users -G audio -s /bin/bash user  
    passwd user  

    gpasswd -a user disk
    gpasswd -a user input
    gpasswd -a user lp
    gpasswd -a user optical
    gpasswd -a user scanner
    gpasswd -a user storage
    gpasswd -a user kvm
    gpasswd -a user video

    groups user

    EDITOR=nano visudo
--
    admin ALL=(ALL:ALL) ALL  
--

    cp /proc/mounts /etc/fstab  
  
    xbps-install grub  
    grub-install /dev/sda  
  
    xbps-install git opendoas


    xbps-reconfigure -fa  
    exit  
    reboot  

    sudo ln-s /etc/sv/dhcpcd /var/service
    sudo sv up dhcpcd

    AUTOLOGIN & AUTOSTART

    sudo nano  /etc/sv/agetty-tty1/conf  
    GETTY_ARGS="-a username --noclear"  
  
    nano .bash_profile  
    [ -f $HOME/.bashrc ] && . $HOME/.bashrc  
    if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then  
    exec startx  
    fi
